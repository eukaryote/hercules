FROM golang:1.10.3 AS builder

COPY . /go/src/gitlab.com/semkodev/hercules
WORKDIR /go/src/gitlab.com/semkodev/hercules

RUN go get
RUN go build -v -o build/hercules ./hercules.go
RUN strip build/hercules


FROM debian:9.4-slim

COPY --from=builder /go/src/gitlab.com/semkodev/hercules/build/hercules /app/
WORKDIR /app
VOLUME /app/db

ENTRYPOINT ["./hercules", "--config=\"hercules.config.json\""]
